import { Component, OnInit, HostListener } from '@angular/core';
import { Eventos, Eventos2 } from 'src/app/models/eventos';
import { EventosService } from 'src/app/service/eventos.service';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  objetoEventosJson: any[] = [];
  eventos: Eventos[] = [];
  mostrarTipoEvento: boolean = false;
  mostrarCiudadEvento: boolean = false;
  mostrarFechaEvento: boolean = false; // Nuevo booleano para mostrar el filtro por fecha
  eventosMostrados: number = 12;
  tiposEvento: string[] = ['Recital', 'Bienestar', 'Peña', 'Deportes', 'Fiesta/Disco', 'Tributo', 'Teatro'];
  ciudades: string[] = ['Córdoba', 'Buenos Aires', 'Catamarca', 'La Rioja', 'Santa Fe', 'San Luis', 'Salta', 'La Pampa'];
  filtrosSeleccionados: string[] = [];
  fechaEspecifica: string = ''; // Nueva variable para la fecha específica
  fechaInicio: string = ''; // Nueva variable para la fecha de inicio del rango
  fechaFin: string = ''; // Nueva variable para la fecha de fin del rango
  // crea una variable ne donde pueda ingresar un json de eventos


  constructor(
    private eventosService: EventosService
  ) {
    this.objetoEventosJson = Eventos2;
  }



  ngOnInit(): void {
    console.log(this.objetoEventosJson);
    console.log(typeof this.objetoEventosJson);
    console.log(this.objetoEventosJson[0].nombre);
    this.eventos = this.objetoEventosJson;
    this.ordenarEventosPorFecha();

    this.checkScroll();

    
  }

  

  
  aplicarFiltroPorFecha() {
    console.log('ingresa al filtro por fecha');
    console.log(this.fechaEspecifica);
    
    if (this.fechaEspecifica) {
      console.log('fecha exacta no es null');
      // Convertir la fecha seleccionada a un formato compatible con las fechas de los eventos
      const fechaEspecificaString = this.fechaEspecifica.toString().slice(0, 10);
  
      // Filtrar eventos por fecha específica
      this.eventos = this.objetoEventosJson.filter(evento => {
        const fechaEvento = evento.inicio_funcion.slice(0, 10); 
        return fechaEvento === fechaEspecificaString;
      });
    } else if (this.fechaInicio && this.fechaFin) {
      console.log('fecha inicio y fecha fin no son null');
  
      // Convertir las fechas seleccionadas a un formato compatible con las fechas de los eventos
      const fechaInicioString = this.fechaInicio.toString().slice(0, 10);
      const fechaFinString = this.fechaFin.toString().slice(0, 10);
  
      // Filtrar eventos por rango de fechas
      this.eventos = this.objetoEventosJson.filter(evento => {
        const fechaEvento = evento.inicio_funcion.slice(0, 10); 
        return fechaEvento >= fechaInicioString && fechaEvento <= fechaFinString;
      });
    }
  
    this.ordenarEventosPorFecha(); // Ordenar los eventos filtrados por fecha
    console.log('resultado de aplicar el filtro');
    console.log(this.eventos);
  }

  


  ordenarEventosPorFecha() {
    this.eventos.sort((a, b) => {
      let dateA = new Date(a.inicio_funcion).getTime();
      let dateB = new Date(b.inicio_funcion).getTime();
      return dateA - dateB;
    });
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.checkScroll();
  }


  checkScroll() {
    const header = document.getElementById('mainHeader');
    if (header) {
      const nav = header.querySelector('.navbar');
      if (nav) {
        if (window.scrollY > 170) {
          nav.classList.remove('opacity-50');
          nav.classList.add('opacity-100');
        } else {
          nav.classList.remove('opacity-100');
          nav.classList.add('opacity-50');
        }
      }
    }
  }

  buscarEventos() {
    this.eventosService.getEventos().subscribe((data: any) => {
      this.eventos = data;
    });
  }

  obtenerTodosEventos() {

    this.eventos = this.objetoEventosJson;
    console.log(this.eventos);

  }

  obtenerEvento(evento: Eventos) {
    console.log(evento);
  }


  /* filtrarEventos(value: string) {
    this.eventos = this.objetoEventosJson.filter(evento => evento.provincia === value);
    console.log(this.objetoEventosJson);
    console.log(this.eventos);
  } */

  filtrarPorTipo(actividad: string) {
    this.eventos = this.objetoEventosJson.filter(evento => evento.actividad === actividad);
    this.ordenarEventosPorFecha(); // Ordenar los eventos filtrados por fecha
    console.log(this.eventos);
  }

  filtrarPorCiudad(ciudad: string) {
    let eventosFiltrados = this.objetoEventosJson.filter(evento => evento.provincia === ciudad);
    this.eventos = eventosFiltrados;
    this.ordenarEventosPorFecha(); // Ordenar los eventos filtrados por fecha
    console.log(eventosFiltrados);
  }

  toggleTipoEvento() {
    this.mostrarTipoEvento = !this.mostrarTipoEvento;
    if (this.mostrarTipoEvento) {
      this.mostrarCiudadEvento = false;
      this.mostrarFechaEvento = false;
      this.fechaEspecifica = ''; 
      this.fechaInicio = ''; 
      this.fechaFin = ''; 
    }
  }

  toggleCiudadEvento() {
    this.mostrarCiudadEvento = !this.mostrarCiudadEvento;
    if (this.mostrarCiudadEvento) {
      this.mostrarTipoEvento = false;
      this.mostrarFechaEvento = false;
      this.fechaEspecifica = ''; 
      this.fechaInicio = ''; 
      this.fechaFin = ''; 
    }
  }
  
  toggleFechaEvento() {
    this.mostrarFechaEvento = !this.mostrarFechaEvento;
    if (this.mostrarFechaEvento) {
      this.mostrarTipoEvento = false;
      this.mostrarCiudadEvento = false;
    }
  }

  
  mostrarMasEventos() {
    this.eventosMostrados += 12;
  }

  
  hayMasEventosPorMostrar(): boolean {
    return this.eventosMostrados < this.eventos.length;
  }
  mostrarInformacion() {
    console.log('boton info');
  }

  comprarEntrada() {
    console.log('boton comprar');
  }



}
