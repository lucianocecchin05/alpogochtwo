import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';

const baseUrl = 'src/';
@Injectable({
  providedIn: 'root'
})
export class EventosService {

constructor(
  private http: HttpClient
) { }
eventos:any = [];
getEventos(){

  return this.http.get(baseUrl + 'eventos.json') // Simula la consulta a la API
      .pipe(
        map((data: any) => {
          this.eventos = data;
          console.log(this.eventos);
           // Almacenar los datos en la propiedad 'eventos'
          return this.eventos; // Devolver los datos
        })
      );

}

}
