import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'alPogoBootstrap';

  yourFunction(){
    console.log('Your function works');
    
  }
}
